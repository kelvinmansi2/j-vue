import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import routes from './routes/index'
Vue.config.productionTip = false

new Vue({
  router: routes,
  store,
  render: h => h(App),
}).$mount('#app')
