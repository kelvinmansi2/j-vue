import Vue from 'vue'//importing vue module
import router from 'vue-router'//importing vue-router module 
import Login from '../components/login-account.vue'
import Reset from '../components/reset-account.vue'
import createAccount from '../components/create-account.vue'
import dashboard from "../components/dashboard.vue";
import account from '../components/sub-class/account.vue'
// import home from '../components/sub-class/home.vue'

Vue.use(router)

export default new router({
    routes: [
        {
            path:'/',
            component: Login
        },
        {
            path:'/reset-account',
            component: Reset
        },
        {
            path:'/create-account',
            component: createAccount
        },
        {
            path:'/dashboard',
            component: dashboard,
            children:[
                {
                    path: '/account',
                    component: account
                }
            ]
        },
    ]
})