import axios from 'axios'///axios module

const api = 'http://localhost:9000/api'//base api url

let userToken = sessionStorage.getItem('user-token')//getting user token from localSession storage

const instance = axios.create()

instance.defaults.timeout = 25000;
instance.defaults.headers.common['Authorization'] = userToken;
instance.defaults.baseURL = api

export {
    instance
}