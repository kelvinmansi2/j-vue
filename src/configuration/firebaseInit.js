import firebase from 'firebase'
import 'firebase/auth'

import firebaseConfiguration from './firebase'

const app = firebase.initializeApp(firebaseConfiguration)
const store = app.firestore()

export {
    app, store
}