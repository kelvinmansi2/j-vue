import { store } from '../../configuration/firebaseInit' ///firebase auth instance
import { instance } from './../../api/axiosInstance'
const collectionString = store.collection("articles").doc("fuu26GimmqJF1WtyhpWg");//firestore collection string

const state = {
    article: [],
}

const getters = {
    setArticle: state => state.article,
}

const actions = {

    getAll: async function(context){
            const article = {}
            await collectionString.get().then(doc=>{
            const { ...docz } = doc.data()
            article.push(...docz)
            context.commit('setArticle', article)    
        return
        })        
        
    },

    sendArticle: async function(context){
            instance({
                method:'get',
                url:'/ws'
            }).then( res=>{
               let watsonOject = {}
               const { ...docz } = res.data
               watsonOject.push(...docz)
               context.commit('setArticle', watsonOject)  
            })
    } 

}

const mutations = {
    setArticle: (state, data) => {
        state.article.push(data)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
