import { app } from '../../configuration/firebaseInit' ///firebase auth instance
// 

const state = {

    isLoading: null,
    msg: null,
    user: {
        token: null,
        userId:null,
        name:null,
        email:null,
        isVerified:null,
        isAuth:null,
    },

    watson: []
}

const getters = {
    getCurrentUser: state => state.user,
    getErrorState: state => state.msg,
    getLoader: state => state.isLoading,
}

const actions = {
        ///handles firebase 'signInWithEmailAndPassword' method
      login: async function(context, data){
        context.commit('setLoader',true)
        const { email, password } = data
        let fireResponse = await app.auth().signInWithEmailAndPassword(email,password)
            .then( response =>{
                app.auth().currentUser.getIdToken(true).then(
                    userToken => {
                        sessionStorage.setItem('user-token', userToken)      
                        context.commit('setLoader', false)                 
            }
                    ).catch(error=>{
                        context.commit('setLoader', false)
                        context.commit('setMsg', error.message)
                    })
                    return response
            }).catch(
                    error=> {
                        context.commit('setLoader', false)
                        context.commit('setMsg', error.message)
                }
            );

        // context.commit('setLoader', false)
        context.commit('setUser', fireResponse)//emit user object to mutation
    },
     ///handles firebase register 'createUserWithEmailAndPassword' method.
    regsiter: async function(context, data){
        const { email, password } = data

        let fireResponse = await app.auth().createUserWithEmailAndPassword(email,password)
            .then( response =>
            { 
                return response.user
                    }).catch(
                        error =>{
                            return error.message
            });
        console.log(fireResponse.user)
    },

    reset: async function(context, data) {
        const { email } = data
        let fireResponse = await app.auth().sendPasswordResetEmail(email)
        console.log(fireResponse)
    }

}

const mutations = {

    setUser: (state, data) => {
            const { email, uid, emailVerified } = data.user
            state.user = {
                userId: uid,
                name: email,
                email: email,
                isVerified: emailVerified,
                isAuth: true
            }
    },
    setMsg: (state, data) => {
        state.msg = data
    },
    setLoader: (state, data) =>{
        state.isLoading = data
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}