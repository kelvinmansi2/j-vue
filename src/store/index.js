import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user'
import articles from './modules/articles'
///loading vue x
Vue.use(Vuex)

//creating store

export default new Vuex.Store({
    modules:{
        user,
        articles
    }
})